// https://transform.tools/json-to-typescript
// 自动生成

export interface SCFHttpEvent {
  body: string
  headers: Headers
  httpMethod: string
  isBase64Encoded: boolean
  path: string
  queryStringParameters: QueryStringParameters
  requestContext: RequestContext
}

export interface SCFFaaSEvent {
  data: string;
  method: string;
  userInfo: UserInfo;
}

interface UserInfo {
  appId: string;
  openId: string;
}


interface Headers {
  accept: string
  'accept-encoding': string
  'cache-control': string
  connection: string
  'content-length': string
  'content-type': string
  host: string
  'postman-token': string
  'user-agent': string
  'x-client-proto': string
  'x-client-proto-ver': string
  'x-forwarded-for': string
  'x-forwarded-proto': string
  'x-real-ip': string
  'x-stgw-time': string
}

interface QueryStringParameters { }

interface RequestContext {
  appId: number
  envId: string
  requestId: string
  uin: number
}

export interface ReturnReportResponse {
  form: string
  form_name: string
  entry: Entry
}

/**
 * 字段名称	数据类型	API Code
序号	Float	serial_number
学生姓名	String	field_2
院系班级名	String	field_3
手机	String	field_4
预计返校日期	Date	field_5
当前所在地区	Hash	field_6
预计返校时的交通信息：	String	field_7
车次/航班号/车牌号/车辆信息	String	field_8
补充其他交通信息	String	field_10
14天内到达过的城市，包括返乡、离校后的城市：	String	field_11
14天内是否去过/途径武汉？	String	field_12
14天内是否接触过发热病人或从武汉出差回来（或途径武汉）的人？	String	field_13
健康状况描述：	String	field_14
所在地是否出现任何与疫情相关的、值得注意的情况?	String	field_15
扩展属性	String	x_field_1
微信昵称	String	x_field_weixin_nickname
微信性别	String	x_field_weixin_gender
微信国家	String	x_field_weixin_country
微信省市	Hash	x_field_weixin_province_city
微信OpenID	String	x_field_weixin_openid
微信头像	String	x_field_weixin_headimgurl
提交人	String	creator_name
提交时间	DateTime	created_at
修改时间	DateTime	updated_at
填写时长	Float	info_filling_duration
IP	String	info_remote_ip
 */

interface Entry {
  serial_number: number
  field_2: string
  field_3: string
  field_4: string
  field_5: string
  field_6: Field6
  field_7: string
  field_8: string
  field_10: string
  field_11: string
  field_12: string
  field_13: string
  field_14: string
  field_15: string
  x_field_weixin_nickname: string
  x_field_weixin_gender: string
  x_field_weixin_country: string
  x_field_weixin_province_city: XFieldWeixinProvinceCity
  x_field_weixin_openid: string
  x_field_weixin_headimgurl: string
  creator_name: string
  created_at: string
  updated_at: string
  info_remote_ip: string
}

interface Field6 {
  longitude: number
  latitude: number
  address: string
}

interface XFieldWeixinProvinceCity {
  province: string
  city: string
}

export interface DailyReportResponse {
  form: string;
  form_name: string;
  entry: DailyReportEntry;
}

export interface DailyReportEntry {
  serial_number: number;
  field_1: string;
  field_2: string;
  field_3: string;
  field_4: string;
  field_6: string;
  field_8: string;
  field_5: string;
  field_7: string[];
  field_9: string;
  creator_name: string;
  created_at: string;
  updated_at: string;
  info_remote_ip: string;
}
